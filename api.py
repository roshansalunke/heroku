import pymysql

from flask import Flask, jsonify, request
from flask_cors import CORS
import pymysql

app = Flask(__name__)
cors = CORS(app)

@app.route('/users')
def get_users():
    return "Hello World";

if __name__ == "__main__":
    app.run(debug=True);
